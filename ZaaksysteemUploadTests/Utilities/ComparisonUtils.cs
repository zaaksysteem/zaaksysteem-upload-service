﻿#region Software License
/*
    Copyright (c) 2015, Mintlab B.V. and all the persons listed in the
    CONTRIBUTORS file.

    Zaaksysteem Upload Service uses the EUPL license, for more information
    please have a look at the LICENSE file.
*/
#endregion

using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace ZaaksysteemUploadTests.Utilities
{
    public static class ComparisonUtils
    {
        public static bool StringListEquals(List<string> first, List<string> second)
        {
            if (NullEquals(first, second))
                return true;

            return Enumerable.SequenceEqual(first, second);
        }

        public static bool StringCollectionEquals(StringCollection first, StringCollection second)
        {
            if (NullEquals(first, second))
                return true;

            return Enumerable.SequenceEqual(first.Cast<string>().ToList(), second.Cast<string>().ToList());
        }

        private static bool NullEquals(object first, object second)
        {
            if ((first == null && second != null)
                  || (first != null && second == null))
                return false;

            if (first == null && second == null)
                return true;

            return false;
        }

    }
}
